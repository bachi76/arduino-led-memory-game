
// LED-Game: Repeat the LED sequences
// by Bachi

  int LED[] = {2, 3, 4};
  int GREEN = 7;
  
  int BTN[] = {14, 15, 16};
  
  int POTI = A5;
  
  int currLevel = -1; 
  const int maxLevel = 100;
  int LEVEL[maxLevel];
  float speed = 1.0f;
  

void setup() {
  Serial.begin(9600);
  Serial.println("App started!");

  pinMode(LED[0], OUTPUT);
  pinMode(LED[1], OUTPUT);
  pinMode(LED[2], OUTPUT);
  
  pinMode(GREEN, OUTPUT);
  
  pinMode(BTN[0], INPUT_PULLUP);
  pinMode(BTN[1], INPUT_PULLUP);
  pinMode(BTN[2], INPUT_PULLUP);
  
  pinMode(POTI, INPUT);
  
  boolean result;
  
  signalOK();
  debug("Starting the game...");
}

void loop() {
  
  debug("Potentiometer value: " + (String) analogRead(POTI));
  
  gameAdjustSpeed();
  gameCreateNextLevel();
  gameShowSequence();
  if (gameGetResponse()) {
    gameLevelWon();
    if (currLevel == maxLevel) {
      gameReset();    
    }
  } else {
    gameOver();
    gameReset();
  }
  
  delay(1000*speed);
  
}

void gameCreateNextLevel() {
  currLevel ++;
  LEVEL[currLevel] = random(3); // 0-2
  debug("Adding Level " + (String) currLevel + ": " + (String) LEVEL[currLevel]);
}

void gameShowSequence() {
  debug("Showing sequence");
  for (int i=0; i<=currLevel; i++) {
    digitalWrite(LED[LEVEL[i]], HIGH);
    delay (1000*speed);
    digitalWrite(LED[LEVEL[i]], LOW);
    delay (300*speed);
    Serial.print(LEVEL[i]);
  }
  Serial.println();
}

boolean gameGetResponse() {
  debug("getting player's response");
  for (int i=0; i<=currLevel; i++) {
    int key = input();
    if (key != LEVEL[i]) return false;
    debug("key ok");  
  }
  debug("well done.");
  return true;
}


void gameLevelWon() {
  signalOK();
}

void gameOver() {
  debug("GAME OVER!");
  signalError();
  delay(1500);
}

void gameReset() {
  debug("Restarting the game...");
  currLevel = -1;
  randomSeed(analogRead(A4) + millis());
}

void gameAdjustSpeed() {
    int setting = map(analogRead(POTI), 0, 1023, 0, 100);
    speed = 0.2 + (setting / 100.0  * 2.0);
    debug("Speed setting: " + (String) setting + ", game factor (x 100): " + (int) speed * 100); 
}


  
void signalInput(int key) {
  digitalWrite(LED[key], HIGH);
  delay(100);
  digitalWrite(LED[key], LOW);
}


  
void signalOK() {
  for (int i=0; i<10; i++) {
    digitalWrite(GREEN, HIGH);
    delay(100*speed);
    digitalWrite(GREEN, LOW);
    delay(100*speed);
  }
  
}

  
void signalError() {
  for (int i=0; i<20; i++) {
    digitalWrite(LED[0], HIGH);
    digitalWrite(LED[2], HIGH);
    delay(100*speed);
    digitalWrite(LED[0], LOW);
    digitalWrite(LED[2], LOW); 
    delay(100*speed);
  }
}

/** Waits for a button press and returns the button number (1-3) **/
int input() {
  int key = -1;
  while (key==-1) {
    key = getButtonState();
    delay(10);
   }
   signalInput(key);
   while (getButtonState() != -1) {
     delay(10);
   }
   
   return key; 
}

/** Get the currently pressed button (0-n), or -1 if none is pressed **/
int getButtonState() {
  int key = -1;
  int size = sizeof(BTN) / sizeof(int);
  for (int i=0; i<size; i++) {
      if (digitalRead(BTN[i]) == LOW) key=i;
  }
  return key;
}

void debug(String msg) {
  Serial.println(msg);  
}


