--- LED MEMORY GAME ---

A simple Arduino example of a LED memory game.
See it in action: http://youtu.be/jpXiVPAxINE

Includes the Arduino sketch code and a Fritzing sketch.
Enjoy.

--
http://gplus.to/bachi


